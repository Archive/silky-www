#
# This copies modified files to the web server, and removes unused files from the server
#

# uncomment the following if you want to do a dry run
# DRYRUN = -n


all:
	@echo Usage: "USER=foobar make install"

clean:
	rm i18n.html

i18n:
#	perl i18n.pl > i18n.html

install: i18n
	rsync $(DRYRUN) -ave ssh --delete *.css *.html *.shtml img patches $(USER)@shell.sourceforge.net:silky/htdocs/

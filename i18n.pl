#!/usr/bin/perl -w

#
# This script is used as follows: ./i18n.pl > i18n.html
# This is usually run from Makefile, by issuing "make i18n" -command.
#
# This code is Public Domain.
#
# (C) Toni Willberg <toniw@iki.fi>
#

use strict;
use POSIX;


my $podir = $ARGV[1] || "../silky/po";
my $wwwdir = $ARGV[2] || `pwd`;

my @pofiles = `ls $podir/*.po`;

my @status = `pushd ../silky/po; intltool-update -r 2>&1 ; popd`;


my $mult = 400; # total width of bar
my $height = 15; # total height of bar
print qq(
	 <table border="0" cellpadding="2" colspacing="2">
	 <th>Language</th> <th>Status</th>
	 <th>Translated</th> <th>Fuzzy</th> <th>Untranslated</th>
	 );

my $transn; my $fuzzyn; my $untransn;
foreach my $langl (@status) {

    if ($langl =~ /^(.+): .*message.*/ ) {
	my $lang = $1;

	if ($langl =~ / (\d+) translated message/g) {
	    $transn = $1;
	} else {
	    $transn = 0;
	}

	if ($langl =~ / (\d+) fuzzy translation/g) {
	    $fuzzyn = $1;
	} else {
	    $fuzzyn = 0;
	}


	if ($langl =~ / (\d+) untranslated message/g) {
	    $untransn = $1;
	} else {
	    $untransn = 0;
	}


	my $total = ceil($transn+$fuzzyn+$untransn);

	my $transp = ceil($transn/$total*100);
	my $transw = ceil($transn/$total*$mult) || 0;

	my $fuzzyp = ceil($fuzzyn/$total*100);
	my $fuzzyw = ceil($fuzzyn/$total*$mult) || 0;

	my $untransp = ceil($untransn/$total*100);
	my $untransw = ceil($untransn/$total*$mult) || 0;

	print qq(
		 <tr>
		  <td>$lang</td>
		  <td>
		   <img src="img/bar0.gif" height="$height" width="$transw" alt="$transn translated"><img src="img/bar4.gif" height="$height" width="$fuzzyw" alt="$fuzzyn fuzzy"><img src="img/bar1.gif" height="$height" width="$untransw" alt="$untransn untranslated">
		  </td>
		  <td align="right">$transn <small>($transp%)</small></td> 
		  <td align="right">$fuzzyn <small>($fuzzyp%)</small></td> 
		  <td align="right">$untransn <small>($untransp%)</small></td>
		 </tr>
		 );
    }
	
}

print qq(
	 </table>
	 );

